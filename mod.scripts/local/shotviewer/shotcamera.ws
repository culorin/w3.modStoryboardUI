// ----------------------------------------------------------------------------
statemachine class CStoryBoardShotCamera extends CStaticCamera {
    // ------------------------------------------------------------------------
    protected var settings: SStoryBoardCameraSettings;

    protected var comp: CCameraComponent;
    protected var env: CEnvironmentDefinition;
    protected var gameDofSettings: SStoryBoardCameraDofSettings;
    // ------------------------------------------------------------------------
    event OnSpawned(spawnData: SEntitySpawnData) {
        var world: CGameWorld;
        comp = (CCameraComponent)this.GetComponentByClassName('CCameraComponent');
        world = (CGameWorld)theGame.GetWorld();
        env = world.environmentParameters.environmentDefinition;
    }
    // ------------------------------------------------------------------------
    public function setSettings(newSettings: SStoryBoardCameraSettings) {
        settings = newSettings;
        // defaults
        if (settings.dofFocalLength == 0.0) { settings.dofFocalLength = 28.2495; }
        if (settings.dofDistance == 0.0) {    settings.dofDistance = 1.27007; }
        if (settings.dofPlane == "") {        settings.dofPlane = "Medium"; }
    }
    // ------------------------------------------------------------------------
    public function getSettings() : SStoryBoardCameraSettings {
        return settings;
    }
    // ------------------------------------------------------------------------
    public function switchTo(optional tempSettings: SStoryBoardCameraSettings) {
        var i: SStoryBoardCameraSettings;

        if (!IsRunning()) {
            this.Run();
        }
        if (tempSettings != i) {
            if (tempSettings.dofFocalLength == 0.0) { tempSettings.dofFocalLength = 28.2495; }
            if (tempSettings.dofDistance == 0.0) {    tempSettings.dofDistance = 1.27007; }
            if (tempSettings.dofPlane == "") {        tempSettings.dofPlane = "Medium"; }
            applySettings(tempSettings);
        } else {
            applySettings(settings);
        }
    }
    // ------------------------------------------------------------------------
    protected function getDof() : SStoryBoardCameraDofSettings {
        // don't even think to copy only CEnvDepthOfFieldParameters into local
        // var -> game freezes
        return SStoryBoardCameraDofSettings(
            env.envParams.m_depthOfField.intensity.dataCurveValues[0].lue,
            env.envParams.m_depthOfField.nearBlurDist.dataCurveValues[0].lue,
            env.envParams.m_depthOfField.farBlurDist.dataCurveValues[0].lue,
            env.envParams.m_depthOfField.nearFocusDist.dataCurveValues[0].lue,
            env.envParams.m_depthOfField.farFocusDist.dataCurveValues[0].lue
        );
    }
    // ------------------------------------------------------------------------
    protected function setDof(dof: SStoryBoardCameraDofSettings) {
        env.envParams.m_depthOfField.intensity.dataCurveValues[0].lue = ClampF(dof.strength, 0, 1);
        env.envParams.m_depthOfField.nearBlurDist.dataCurveValues[0].lue = ClampF(dof.blurNear, 0, 100);
        env.envParams.m_depthOfField.farBlurDist.dataCurveValues[0].lue = ClampF(dof.blurFar, 0, 500);
        env.envParams.m_depthOfField.nearFocusDist.dataCurveValues[0].lue = ClampF(dof.focusNear, 0, 100);
        env.envParams.m_depthOfField.farFocusDist.dataCurveValues[0].lue = ClampF(dof.focusFar, 0, 500);
    }
    // ------------------------------------------------------------------------
    protected function applySettings(s: SStoryBoardCameraSettings) {
        // only apply to camera but do NOT transfer to settings attribute!
        this.TeleportWithRotation(s.pos, s.rot);
        this.comp.fov = ClampF(RoundF(s.fov), 1, 150);
        this.SetZoom(ClampF(s.zoom, 1, 5));
        this.setDof(settings.dof);
    }
    // ------------------------------------------------------------------------
    public function setPosition(value: Vector) {
        settings.pos = value;
        this.TeleportWithRotation(settings.pos, settings.rot);
    }
    // ------------------------------------------------------------------------
    public function setRotation(value: EulerAngles) {
        settings.rot = value;
        this.TeleportWithRotation(settings.pos, settings.rot);
    }
    // ------------------------------------------------------------------------
    public function setFov(value: float) {
        settings.fov = ClampF(RoundF(value), 1, 150);
        this.comp.fov = settings.fov;
    }
    // ------------------------------------------------------------------------
    public function setZoom(value: float) {
        settings.zoom = ClampF(value, 1, 5);
        this.SetZoom(settings.zoom);
    }
    // ------------------------------------------------------------------------
    public function setDofIntensity(value: float) {
        settings.dof.strength = ClampF(value, 0, 1);
        this.setDof(settings.dof);
    }
    // ------------------------------------------------------------------------
    public function setBlur(near: float, far: float) {
        settings.dof.blurNear = ClampF(near, 0, 100);
        settings.dof.blurFar = ClampF(far, 0, 500);
        this.setDof(settings.dof);
    }
    // ------------------------------------------------------------------------
    public function setFocus(near: float, far: float) {
        settings.dof.focusNear = ClampF(near, 0, 100);
        settings.dof.focusFar = ClampF(far, 0, 500);
        this.setDof(settings.dof);
    }
    // ------------------------------------------------------------------------
    public function setAperture(dofFocalLength: float, dofDistance: float) {
        settings.dofFocalLength = ClampF(dofFocalLength, 1.0, 150.0);
        settings.dofDistance = ClampF(dofDistance, 0.1, 20.0);
    }
    // ------------------------------------------------------------------------
    public function setDofPlane(plane: String) {
        settings.dofPlane = plane;
    }
    // ------------------------------------------------------------------------
    public function activate() {
        this.Run();
        gameDofSettings = getDof();
        applySettings(settings);
    }
    // ------------------------------------------------------------------------
    public function deactivate() {
        this.setDof(gameDofSettings);
        this.Stop();
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
